using Microsoft.Extensions.Logging;
using System;
using System.Timers;

namespace standup.Services
{
    public class StandupService : IStandupService
    {
        private int _time = 45;

        public int Time
        {
            get => _time;
            set
            {
                _time = value;
                TimeUpdated?.Invoke(this, null);
            }
        }

        private int _page = 0;
        public int Page
        {
            get => _page;
            set
            {
                _page = value;
                PageUpdated?.Invoke(this, null);
            }
        }

        public event EventHandler TimeUpdated;

        public event EventHandler PageUpdated;

        private readonly ILogger<StandupService> _logger;

        private Timer _timer = new Timer
        {
            AutoReset = true,
            Interval = 1000
        };

        public StandupService(ILogger<StandupService> logger)
        {
            _logger = logger;
        }

        public void StartTimer()
        {
            _logger.LogTrace("Start message arrived.");
            if(!_timer.Enabled)
            {
                _timer.Elapsed += OnTimedEvent;
                _timer.Start();
            }
        }

        public void StopTimer()
        {
            _logger.LogTrace("Stop message arrived.");
            if(_timer.Enabled)
            {
                _timer.Stop();
                _timer.Elapsed -= OnTimedEvent;
                Time = 45;
            }
        }

        public void NextPage()
        {
            Page = (Page + 1) % 3;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (Time > 0)
            {
                Time -= 1;
            }
        }
    }
}
