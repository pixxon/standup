using System;

namespace standup.Services
{
    public interface IStandupService
    {        
        int Time { get; set; }

        int Page { get; set; }

        event EventHandler TimeUpdated;
        
        event EventHandler PageUpdated;

        void StartTimer();

        void StopTimer();

        void NextPage();
    }
}
