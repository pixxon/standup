"use strict";

class Standup
{
    constructor(page, time)
    {
        var self = this;

        self.page = page;
        self.time = time;

        self.pages = [ $("#su1"), $("#su2"), $("#su3") ];
        self.pages[self.page].show();
        
        self.start = $("#startButton");
        self.stop = $("#stopButton");
        self.next = $("#nextButton");

        self.connection = new signalR.HubConnectionBuilder().withUrl("/standupHub").build();
        
        self.connection.on("TimeUpdate", function(time)
        {
            self.time = time;
            $("#remaining").text(self.time);
        });

        self.connection.on("PageUpdate", function(page)
        {        
            self.pages[self.page].hide();
            self.page = page - 1;
            self.pages[self.page].show();
        });

        self.connection.start().then(function()
        {
            self.start.disabled = false;
            self.stop.disabled = false;
            self.next.disabled = false;
        }).catch(function(err)
        {
            return console.error(err.toString());
        });
        
        self.start.click(function()
        {
            self.connection.invoke("StartTimer").catch(function(err)
            {
                return console.error(err.toString());
            });
            event.preventDefault();
        });
        
        self.stop.click(function()
        {
            self.connection.invoke("StopTimer").catch(function(err)
            {
                return console.error(err.toString());
            });
            event.preventDefault();
        });
        
        self.next.click(function()
        {
            self.connection.invoke("NextPage").catch(function(err)
            {
                return console.error(err.toString());
            });
            event.preventDefault();
        });

        $("body").click(function()
        {
            if (self.time == 45)
            {      
                self.connection.invoke("StartTimer").catch(function(err)
                {
                    return console.error(err.toString());
                });
            }
            else if (self.page == 2)
            {       
                self.connection.invoke("StopTimer").catch(function(err)
                {
                    return console.error(err.toString());
                });

                self.connection.invoke("NextPage").catch(function(err)
                {
                    return console.error(err.toString());
                });
            }
            else 
            {
                self.connection.invoke("NextPage").catch(function(err)
                {
                    return console.error(err.toString());
                });  
            }
            event.preventDefault();
        });

		//self.pb = new ProgressBar.Path('#heart-path', {
		//	duration: 45 * 1000,
		//});
    }
}
