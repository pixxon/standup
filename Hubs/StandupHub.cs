using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System.Timers;
using System;
using standup.Services;
using Microsoft.Extensions.Logging;

namespace standup.Hubs
{
    public class StandupHub : Hub
    {
        private IStandupService _service;

        private ILogger<StandupHub> _logger;

        public StandupHub(IStandupService service, ILogger<StandupHub> logger)
        {
            _service = service;
            _logger = logger;
        }

        public void StartTimer()
        {
            _logger.LogTrace("Start message arrived.");
            _service.StartTimer();
        }

        public void StopTimer()
        {
            _logger.LogTrace("Stop message arrived.");
            _service.StopTimer();
        }

        public void NextPage()
        {
            _logger.LogTrace("Naxt page message arrived.");
            _service.NextPage();
        }
    }
}