﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using standup.Services;
using standup.Hubs;

namespace standup.Pages
{
    public class StandupModel : PageModel
    {
        private readonly ILogger<StandupModel> _logger;
        private readonly IStandupService _service;
        private readonly IHubContext<StandupHub> _hub;

        public int Time { get; private set; } = 45;

        public new int Page { get; private set; } = 0;

        public StandupModel(IStandupService service, IHubContext<StandupHub> hub, ILogger<StandupModel> logger)
        {
            _logger = logger;
            _service = service;
            _hub = hub;

            Time = _service.Time;
            Page = _service.Page + 1;
            _service.TimeUpdated += OnTimer;
            _service.PageUpdated += OnPage;
        }

        public void OnGet()
        {
        }

        private async void OnTimer(Object source, EventArgs e)
        {
            Time = _service.Time;
            await _hub.Clients.All.SendAsync("TimeUpdate", Time);
        }

        private async void OnPage(Object source, EventArgs e)
        {
            Page = _service.Page + 1;
            await _hub.Clients.All.SendAsync("PageUpdate", Page);
        }
    }
}
